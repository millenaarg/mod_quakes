%% @author 
%% @copyright 2015 
%% Generated on 2015-09-03
%% @doc This site was based on the 'empty' skeleton.

%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%% 
%%     http://www.apache.org/licenses/LICENSE-2.0
%% 
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.

-module(mod_quakes).
-author("Guido@Millenaar.net").

-mod_title("quakes module for Zotonic").
-mod_description("Shows some statistics and details about earthquakes in the Netherlands").
-mod_prio(10).

-include_lib("zotonic.hrl").
-export([init/1, retrieve_data/0, event/2]).

%%====================================================================
%% support functions go here
%%====================================================================

init(Context) ->
	io:format("quakes:init/1 called ~n"),
	m_module_quakes:init(Context),
	retrieve_data().


%%====================================================================
%% Data retrieval and normalization
%%====================================================================

%% retrieve data. --todo: make url a variable
retrieve_data()->
	{ok, {{_, 200, _}, _, Body}} = httpc:request(get, {"http://www.opengis.eu/geoservice/bevingen.json", []}, [], []),
	JsonData = jiffy:decode(Body, [return_maps]),
	ListData = maps:to_list(JsonData),
	[{_,ItemList}|_] = ListData,
	m_module_quakes:process_data(ItemList).	



%%==============
%% Postbacks
%%==============

event({postback, overview, TriggerId, TargetId},Context) ->
	z_render:update_selector(".container", #render{template="_quakes_overview.tpl"}, Context).
