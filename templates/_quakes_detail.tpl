
{% lib
                        "js/apps/jquery-latest.js"
                        "js/apps/jquery-ui-latest.min.js"
                        "j~s/apps/zotonic-1.0.js"
                        "bootstrap/js/bootstrap.min.js"
                        "css/bootstrap/bootstrap/css/bootstrap.css"
                        "css/style.css"
                        "js/apps/z.widgetmanager.js"
                        "js/modules/ubf.js"
                        "js/modules/z.inputoverlay.js"
                        "js/modules/z.dialog.js"
                        "js/modules/jquery.loadmask.js"
                        "js/modules/chart.min.js"
%}
    <style scoped>
     #map {
	width: 600px;
	height: 400px;
	}
    </style>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
    	<script>
     	function initialize() {
        	var mapCanvas = document.getElementById('map');
		var myLatLng = {lat:{% m.mod_quakes[q.id].lat %},lng:{% m.mod_quakes[q.id].long %},map:map}; 
        	var mapOptions = {
          	center: myLatLng,
          	zoom: 10,
          	mapTypeId: google.maps.MapTypeId.ROADMAP
        	}
        	var map = new google.maps.Map(mapCanvas, mapOptions);
		var marker = new google.maps.Marker({position: myLatLng, map:map});
		}
	
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>

<div class="row quakes-container">
<h3>Detailed information about the earthqake on {{ m.mod_quakes[q.id].date }} in {{ m.mod_quakes[q.id].location }}.</h3> <br>
</div> <!-- closing row -->
<div class="row">
<div class="col-md-5">

<table class="table table-hover">
	<tbody>
	<tr><td>Date: </td><td>{{ m.mod_quakes[q.id].date }}</td</tr>
	<tr><td>Time: </td><td>{{ m.mod_quakes[q.id].time }}</td</tr>
	<tr><td>Magnitude: </td><td>{{ m.mod_quakes[q.id].magnitude }}</td</tr>
	<tr><td>Depth: </td><td>{{ m.mod_quakes[q.id].depth }}</td</tr>
	<tr><td>Location: </td><td>{{ m.mod_quakes[q.id].location }}</td</tr>
	<tr><td>Latitude: </td><td>{{ m.mod_quakes[q.id].lat }}</td</tr>
	<tr><td>Longitude: </td><td>{{ m.mod_quakes[q.id].long }}</td</tr>
	</tbody>
</table>
<br>
<br>
<br>
<br>
&nbsp;<a class="btn btn-default" href="javascript:history.go(-1)" role="button"> <<  Back to overview << </a>
</div> <!-- closing col md 5 -->
<div class="col-md-5">
<div id="map"></div><!-- closing map -->
</div> <!-- closing col md 5 -->
</div> <!-- closing row quakes-container-->
