<!-- todo -- move some js to bottom, remove unwanted js -->
{% lib
           		"js/apps/jquery-latest.js"
			"js/apps/jquery-ui-latest.min.js"
                        "js/apps/zotonic-1.0.js"
                        "bootstrap/js/bootstrap.min.js"
			"css/bootstrap/bootstrap/css/bootstrap.css"
			"css/style.css"
                        "js/apps/z.widgetmanager.js"
                        "js/modules/ubf.js"
                        "js/modules/z.inputoverlay.js"
                        "js/modules/z.dialog.js"
                        "js/modules/jquery.loadmask.js"
			"js/modules/chart.min.js"
%}

<div class="row quakes-container">
	<div class"col-md-12">
	<h1>QUAKES!</h1><h2> we have them... </h2>
	<p>This is a Zotonic module showing some statistics on earthquakes in the Netherlands.<br>
	Built with Zotonic, an Erlang web framework (and also jQuery, Bootstrap and Chart.js).</p>
	Data source used can be found on <a href="http://www.opengis.eu/geoservice/bevingen.json"> opengis.eu</a>. Republished data from the KNMI Seismology department.<br> 
	</div> <!-- closing col md 12 -->
</div> <!-- closing row -->
<div class="row chart-container">
<div class="col-md-10 text-center">
	<script>
	var data = {
    	    labels: [ {% for item in m.module_quakes[4] %}{% for k,v in item %} {% if k =="year" and forloop.parentloop.last =="true" %}"{{ v }}"{% elif k =="year" %}"{{ v }}",{% endif %}{% endfor %}{% endfor %}],
    	    datasets: [ 
            {
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
   	    data: [ {% for item in m.module_quakes[4] %}{% for k,v in item %} {% if k =="count" and forloop.parentloop.last =="true"%}{{ v }}{% elif k =="count" %}{{ v }},{% endif %}{% endfor %}{% endfor %}]
            }]
	};
	</script>
<br>
<h4>Frequency of earthquakes in the Netherlands since 1986</h4>
<canvas id="myChart" width="920" height="200"></canvas> 
<script>
	var ctx = $("#myChart").get(0).getContext("2d");
	
        var myLineChart = new Chart(ctx).Line(data);
</script>
</div> <!-- end col-md-10 -->
</div> <!-- end row chart-container -->

<div class="row">
<div class="col-md-6 top30">
<table class="table table-condensed table-hover top30" style="">
<caption class="text-left"> 30 Highest Magnitude Earthquakes </caption>
        <thead> <tr> <th> Magnitude </th> <th> Location </th> <th> Date </th> <th> Info </th> </tr></thead>
                <tbody>
                {% for item in m.module_quakes[1] %}
                        <tr>{% for k,v in item %}
                                {% if k == "gid" %}
                                <td> <a href={% url detail id = v %}> Details </a></td>
                                {% else %}
                                <td> {{ v }} </td>
                                {% endif %}
                        {% endfor %} </tr>
                {% endfor %}
                </tbody>
</table>

</div> <!-- closing col md 6 top30 -->

<div class="col-md-6 latest30">
<table class="table table-condensed table-hover latest30" style="">
<caption class="text-left"> Latest 30 Earthquakes </caption> 
	<thead> <tr> <th> Magnitude </th> <th> Location </th> <th> Date </th>  <th> Info </th></tr> </thead> 
		<tbody>
		{% for item in m.module_quakes[2] %}
        		<tr>{% for k,v in item %}
                		{% if k == "gid" %}
                        	<td> <a href={% url detail id = v %}> Details </a></td>
                		{% else %}
                        	<td> {{ v }} </td>
                		{% endif %}
        		{% endfor %} </tr>
		{% endfor %}
		</tbody>
</table>
</div> <!-- closing col md 6 latest30 -->

</div> <!-- closing row quakes-container-->

<div class="row text-center">
<hr>
Stonefree, baby! Have a look at the source on <a href="https://bitbucket.org/millenaarg/quakes">BitBucket</a><br>
&nbsp;
</div> <!-- closing row -->
