% Model for quakes 
%
%

-module(m_module_quakes).
-author("guido@millenaar.net").

-behaviour(gen_model).

%% interface functions
-export([init/1, insert_items/2, m_find_value/3, m_to_list/2, m_value/2, query_sanitize/1, process_data/1]).

-include_lib("zotonic.hrl").
-define(table, mod_quakes).

% Check if the table as defined in -define() exists, if not create it
% column_def record from zotonic.hrl: 
% -record(column_def, {name, type, length, is_nullable=true, default, primary_key, unique=false}).


init(Context) ->
        case z_db:table_exists(?table,Context) of
	false ->
		io:format("Inside Query"),
		z_db:create_table(?table, 
		[
		#column_def{name=id, type="serial"},
		#column_def{name=gid, type="integer", is_nullable=false, unique=true},
		#column_def{name=magnitude, type="character varying"},
		#column_def{name=depth, type="real"},
		#column_def{name=location, type="character varying"},
		#column_def{name=time, type="character varying"},
		#column_def{name=date, type="character varying"},
		#column_def{name=year, type="integer"},
		#column_def{name=long, type="double precision"},
		#column_def{name=lat, type="double precision"}
		], Context);
	true -> ok
	end,
	io:format("Outside Query"),
	ok.

%
% Take ListData and normalize
%

process_data(ItemList) ->
	ItemMap = get_maps(ItemList),
	get_maps(ItemMap).


%% Turn items into individual maps

get_maps([]) ->
        done;
get_maps([First|Rest]) ->
        Map = maps:merge(maps:get(<<"geometry">>, First), maps:get(<<"properties">>, First)),
        case is_map(Map) of
                true ->
                        map_to_proplist(Map),
                        get_maps(Rest);
                false -> done
                end,
        done;
get_maps(_) ->
        done.

%% Turn map into proplist, required by z_db:insert in m_quakes
%% All data is in binary strings. binary_to_list --> string:to_[type]

map_to_proplist(Map) ->
        case is_map(Map) of
                true ->
                        {Gid,_}         = string:to_integer(binary_to_list(maps:get(<<"gid">>, Map))),
                        Long            = get_coords(long, maps:get(<<"coordinates">>, Map)),
                        Lat             = get_coords(lat, maps:get(<<"coordinates">>, Map)),
                        Magnitude       = binary_to_list(maps:get(<<"mag">>, Map)),
                        {Depth,_}       = string:to_float(binary_to_list(maps:get(<<"depth">>, Map))),
                        Location        = binary_to_list(maps:get(<<"location">>, Map)),
                        Time            = binary_to_list(maps:get(<<"time2">>, Map)),
                        Date            = binary_to_list(maps:get(<<"datum">>, Map)),
                        {Year,_}        = string:to_integer(string:sub_string(binary_to_list(maps:get(<<"yymmdd">>, Map)),1,4)),
                        PropList = [{gid, Gid}, {magnitude, Magnitude}, {depth, Depth}, {location, Location},
                                    {time, normalize_time(Time)}, {date, Date}, {year, Year}, {lat, Lat}, {long, Long}],
                        m_modules_quakes:insert_items(PropList,z:c(quakes)),
                        ok;
                false ->
                         ok
                end.


%%
%% Helper function to extract latitude and longitude from the coordinates value
%%

get_coords(long, Value) ->
        [H|_] = Value,
        H;
get_coords(lat,Value) ->
        lists:last(Value).

%%
%% Helper function to normalize time format
%%

normalize_time(Time) ->
	Hours   = string:sub_string(Time,1,2),
	Minutes = string:sub_string(Time,3,4),
	Seconds = string:sub_string(Time,5,6),
	string:join([Hours,Minutes,Seconds],":").
	



% Insert earthquake proplist into db. Catch errors for duplicate GID entries(unique constrained)
% 
insert_items(PropList,Context) ->
	(catch z_db:insert(?table, PropList, Context)).

%
% Mandatory gen_model functions
%


%% Find quake by Gid or return a list sorted by time or magnitude.
%% A bit of a hack: because the GID is the identifier and it is 8 digits the logic for 
%% retrievng the lists can be greatly reduced. Todo: proper implementation of the
%% model functions.

m_find_value(1,_,Context) ->
		Query = "SELECT 
			magnitude,location,date,gid 
			FROM quakes ORDER BY magnitude DESC LIMIT 30",
		Result = z_db:assoc(Query, Context),
		Result;
m_find_value(2,_,Context) ->
                Query = "SELECT magnitude,location,date,gid FROM quakes ORDER BY date DESC LIMIT 30",
                Result = z_db:assoc(Query, Context),
                Result;
m_find_value(3,_,Context) ->
                Query = "SELECT year, COUNT(*) FROM quakes GROUP BY year ORDER BY year DESC",
                Result = z_db:assoc(Query, Context),
                Result;
m_find_value(4,_,Context) ->
                Query = "SELECT year, COUNT(*) FROM quakes GROUP BY year ORDER BY year ASC",
                Result = z_db:assoc(Query, Context),
                Result;
m_find_value(Gid,_,Context) ->
	case query_sanitize(Gid) of  %% Some *basic* SQL injection protection because the querystring is used as input
		false -> 
			Result = [{invalid_id, Gid}];
		Sanitized ->
                        GidString = integer_to_list(Sanitized),
                        Query = "SELECT * FROM quakes WHERE gid = " ++ GidString,
                        [Result|_] = z_db:assoc(Query, Context)
		end,
	Result.

m_to_list(_,_) ->
	ok.

m_value(_,_) ->
	ok.


%% Sanitize query string to prevent SQL injection.
%%

query_sanitize(Querystring) ->
		try	
			list_to_integer(Querystring) %% only numbers are used as input, any other characters will not be successfully cast
		of
			_ -> Queryint = list_to_integer(Querystring),
			Queryint
		catch
		_:_ -> false
	end.
			
